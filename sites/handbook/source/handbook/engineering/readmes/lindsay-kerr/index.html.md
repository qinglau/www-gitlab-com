---
layout: markdown_page
title: "Lindsay Kerr's README"
job: "Frontend Engineering Manager, Threat Management"
---

## Lindsay Kerr's README

Hi, I'm Lindsay. I made this README as a way for others to get to know me better. I haven't worked with any of my colleagues at GitLab before, or at this point met them in person, so this README is one way to better introduce myself. 

This is the first README I've ever written, it admittedly feels a little awkward. In an effort to start somewhere, I'm practicing the value of 👣 [iteration](/handbook/values/#iteration) & plan to update this README frequently. 

My role at GitLab is the Frontend Engineering Manager for the Threat Management sub-department. You may ask "why do you identify with a sub-department instead of a group or a stage like most managers do"? Our team spans two groups which are in two different stages of the product. The Container Security group is in the Protect Stage, and the Threat Insights group is in the Secure Stage. Originally both groups were in a stage called Defend, but things change. 


## About me

I live in Eugene, Oregon with my husband, our six year old daughter Audrey, and two cats (Takahata & Miyazaki). If you couldn't guess based on our cat's names, we love watching Studio Ghibli movies. I also enjoy spending time gardening, hiking, kayaking, and cooking. I was born and raised in Oregon. After moving around to a few of difference places (including Chicago & San Fransisco) we settled down & bought our "purple palace" in Eugene. It is actually purple, but it's only a palace to us. 


## My working style

* I subscribe to the [servant-leadership](https://en.wikipedia.org/wiki/Servant_leadership) style. 
* I try to present solutions or suggestions whenever bringing a complaint or concern to a discussion.
* I embrace transparency & asynchronous communication whenever possible.
* It's important to me that the engineers on my team get to solve problems. As a manager, I'm here to clarify, unblock, and cheerlead.
* Scrum and Agile development practices are what brought me into management. I'm passionate about building strong team processes.


## What I assume about others

* I put confidence in other people's positive intentions, people mean well and strive to do their best. 
* We are all on one team working together to achieve common goals.
* Everyone wants to have a little bit of fun at work. 
* Honesty is the foundation of all conversations. 

## Communicating with me

* My working hours are typically 9-5pm PST Monday-Friday.
* I avoid checking Slack & email over the weekend or in the evening. 
* If your topic to discuss relates to something being tracked in an issue or MR, whenever possible start the conversation there.
* Don't hesitate to reach out to me directly if you want to talk about anything personal, sensitive, or confidential.
* I appreciate honesty even when it involves criticism.

### 1:1s

* I may reschedule, you should feel free to too, but I avoid cancelling if at all possible.
* We can talk about most work related topics in the MR, issue, or Slack. 1:1s are best spent getting to know each other better.
* I tend over share about my life. 
* Agendas should be driven by my direct reports. But if you don't add anything, we're still talking.
* I have a tendency of cursing & telling inappropriate stories/jokes.

### Personality quirks

* My face turns red when I'm embarrassed, excited, nervous ... and at other unexpected times. 
* I have a standing desk but can't stay standing (or sitting) for more than 10 minutes at a time, if you're on a long call with me expect to see my desk adjusted multiple times.


## Let's connect on Social Media

* [LinkedIn](https://www.linkedin.com/in/lindsay-a-kerr/)
* [Twittter](https://twitter.com/lkerr78)
* [Instagram](https://www.instagram.com/lkerr_uo/)

