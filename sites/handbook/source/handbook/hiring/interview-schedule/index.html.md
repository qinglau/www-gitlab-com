---
layout: handbook-page-toc
title: "Interview Schedule"
description: "Interview Schedule is a program GitLab uses to increase efficiency in scheduling interviews."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Interview Schedule

Resource Guide is integrated with Greenhouse via API.
DRI: Candidate Experience Team

#### How to schedule 1:1 via Interview Schedule

#### How to schedule technical or behavioral interviews using pools and attributes

#### How to reschedule an interview via Interview Schedule



